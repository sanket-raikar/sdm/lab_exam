CREATE TABLE movie(
    movie_id INTEGER PRIMARY KEY AUTO_INCREMENT,
    movie_title VARCHAR(20), 
    movie_release_date DATE,
    movie_time TIMESTAMP,
    director_name VARCHAR(30)
    );

    INSERT INTO movie(movie_title, movie_release_date, movie_time, director_name) VALUES('spiderman', '2021-01-01', '2021-01-01 00:00:01', 'san');