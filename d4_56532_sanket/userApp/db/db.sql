CREATE TABLE users(
    Id INTEGER PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(20),
    email VARCHAR(30),
    age INTEGER
);

INSERT INTO users(name, email, age) VALUES('san', 'san@gmail.com', 24);
INSERT INTO users(name, email, age) VALUES('man', 'man@gmail.com', 30);
INSERT INTO users(name, email, age) VALUES('ran', 'ran@gmail.com', 26);